// Виведіть цей масив на екран у вигляді списку (тег ul - список повинен бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитися div з id = "root", куди і потрібно буде покласти цей список (схоже завдання була дана в модулі basic).
// Перед виведенням об'єкта на сторінці, потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
// Якщо якогось з цих властивостей немає, в консолі повинна висвітитися помилка із зазначенням - якої якості немає в об'єкті.
// Ті елементи масиву, які є некоректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

"use strict";

document.getElementById("root").className = "block";

const ul = document.createElement("ul");
ul.className = "list";
const rootDiv = document.getElementById("root");
rootDiv.appendChild(ul);

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

for (let key in books) {
  try {
    if (!books[key].author) throw 'We don\'t have "author" property';
    if (!books[key].name) throw 'We don\'t have "name" property';
    if (!books[key].price) throw 'We don\'t have "price" property';
    const li = document.createElement("li");
    li.innerHTML =
      books[key].author + " " + books[key].name + " " + books[key].price;
    ul.appendChild(li);
  } catch (err) {
    console.log(err);
  }
};
